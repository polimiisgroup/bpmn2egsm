BPMN to E-GSM translator written in ATL (ATLAS Transformation Language). To cite this work, please use [3].

~

Purpose:

This project allows to derive from a standard BPMN Process model an E-GSM model [1], extension of the Guard-Stage-Milestone [2] notation, that captures the execution order of activities within the process and the lifecycle of the involved data artifacts. Please refer to [3] for further details and application scenarios.

~

Contents:

BPMN2EGSM - ATL project that, given a BPMN 2.0 Process Diagram, produces an E-GSM Model representing the process in terms of activities and their control flow. Please refer to [4] for details on how the translation is performed.
The source model must be in XMI format. The included XSLT file BPMN20-ToXMI.xsl can be used to convert a source model in BPMN 2.0 XML format to XMI
The target model will be in XMI format. The included XSLT file xmi2siena.xsl can be used to convert the target model in an XML format close to the one used by the BizArtifact system [5] (please note that, being E-GSM a superset of GSM, the target model is currently incompatible with BizArtifact without further transformations).


FSM2EGSM - ATL project that, given an UML 2.0 State Chart Diagram representing the lifecycle of a data artifact, represents that information in E-GSM. The target model will be in XMI format.


it.polimi.isgroup.bpmn2egsmplugin - Java application that automates the translation: given in input a BPMN 2.0 Process Diagram and zero or more UML 2.0 State Chart Diagrams, it produces a ZIP archive with structure close to the one used by BizArtifact exported applications. More in detail, the Zip archive contains (i) an E-GSM representation of both the tasks within the process and their execution flow, together with the lifecycle of the data artifacts (file siena.xml), and (ii) the XML DTD of the Information Model (file data/infoModel.xsd).
The ordering of the application's parameters is the following: processModel.bpmn <lifecycle.uml> application.zip

~

Test models:

BPMN2GSM/shippingProcess.bpmn - A simple shipping process featuring an exclusive block and a backward interrupting boundary event block.

BPMN2GSM/shippingProcess-new.bpmn - A variation of the previous process featuring a loop block and a forward interrupting boundary event block.

BPMN2GSM/caseStudy-merged.bpmn - A process representing the initial phase of a multimodal shipping process. Details can be found in [3]

BPMN2GSM/annotatedProcess.bpmn - Test process featuring conditions on XOR and OR branches.

BPMN2GSM/BoundaryEventsComplexProcess.bpmn - Test process featuring multiple boundary events per Activity (both interrupting and non interrupting)

BPMN2GSM/BoundaryEventsProcess.bpmn - Test process featuring at most one boundary event per Activity

BPMN2GSM/LoopingProcess.bpmn - Test process featuring nested loop patterns

BPMN2GSM/NestedProcess.bpmn - Test process featuring nested subprocesses

BPMN2GSM/WellStructuredProcess.bpmn - Test process featuring a well-structured (block structured) process


FSM2GSM/casestudy.uml - Lifecycle of the container artifact within the process modeled in caseStudy-merged.bpmn

~

Important notes: 

To execute the ATL files within Eclipse, the BPMN and UML metamodels must be loaded in the EMF registry (if BPMN and UML modeling plugins are installed, this operation is automatically carried out). This project has been tested with Eclipse 4.4.2 Luna and Oracle (R) Java (TM) 1.8.0. This project currently does NOT work in Eclipse 4.5 Mars.

~

References:

[1] - Baresi, L., Meroni, G., Plebani, P.: A gsm-based approach for monitoring cross-organization business processes using smart objects. To appear in: BPM Workshops. Springer (2015)

[2] - Hull, R., Damaggio, E., Fournier, F., Gupta, M., Heath, Fenno(Terry), Hobson, S., Linehan, M., Maradugu, S., Nigam, A., Sukaviriya, P., Vaculin, R.: Introducing the guard-stage-milestone approach for specifying business entity lifecycles. In: Web Services and Formal Methods, Lecture Notes in Computer Science, vol. 6551, pp. 1-24. Springer (2011)

[3] - Baresi, L., Meroni, G., Plebani, P.: Using the Guard-Stage-Milestone Notation for Monitoring BPMN-based Processes. Accepted for publication

[4] - Meroni, G., Baresi, L., Plebani, P.: Translating BPMN to E-GSM: specifications and rules. Technical report, Politecnico di Milano (2016), http://hdl.handle.net/11311/976678

[5] - Heath, Fenno(Terry), Boaz, D., Gupta, M., Vaculin, R., Sun, Y., Hull, R., Limonad, L.: Barcelona: A Design and Runtime Environment for Declarative Artifact-Centric BPM. In: Service Oriented Computing, vol. 8274, pp. 705-709. Springer (2013)