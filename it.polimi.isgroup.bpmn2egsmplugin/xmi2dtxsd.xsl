<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ca="http://siena.ibm.com/model/CompositeApplication">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

<xsl:template match="/ca:ComponentType">
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" attributeFormDefault="unqualified" elementFormDefault="qualified" id="{/ca:ComponentType/@id}_DataTypes">
<xs:complexType name="{/ca:ComponentType/@id}">
		<xs:sequence/>
		<xs:attribute name="ID" type="xs:long" use="optional"/>
		<xs:attribute name="CurrentState" use="required"><xs:simpleType><xs:restriction base="xs:string"><xs:enumeration value="Created"/></xs:restriction></xs:simpleType></xs:attribute>
</xs:complexType>
<xs:complexType name="{/ca:ComponentType/@id}Filter">
		<xs:sequence/>
		<xs:attribute name="ID" type="xs:string" use="optional"/>
		<xs:attribute name="CurrentState" type="xs:string" use="optional"/>
</xs:complexType>
<xs:complexType name="ABOStage">
		<xs:sequence>
			<xs:element xmlns:xs="http://www.w3.org/2001/XMLSchema" name="SubStage" type="ABOStage" maxOccurs="unbounded"/>
	</xs:sequence>
		<xs:attribute name="Name" type="xs:string" use="required"/>
		<xs:attribute name="Status" type="xs:string" use="required"/>
		<xs:attribute name="OpenTime" type="xs:dateTime" use="required"/>
		<xs:attribute name="CloseTime" type="xs:dateTime" use="required"/>
		<xs:attribute name="TaskTerminationStatus" type="xs:string" use="required"/>
		<xs:attribute name="TaskTerminationMessage" type="xs:string" use="required"/>
</xs:complexType>
<xs:complexType name="ABOStageFilter">
		<xs:sequence>
			<xs:element xmlns:xs="http://www.w3.org/2001/XMLSchema" name="SubStageFilter" type="ABOStageFilter"/>
	</xs:sequence>
		<xs:attribute name="Name" type="xs:string" use="required"/>
		<xs:attribute name="Status" type="xs:string" use="required"/>
		<xs:attribute name="OpenTime" type="xs:string" use="required"/>
		<xs:attribute name="CloseTime" type="xs:string" use="required"/>
		<xs:attribute name="TaskTerminationStatus" type="xs:string" use="required"/>
		<xs:attribute name="TaskTerminationMessage" type="xs:string" use="required"/>
</xs:complexType>
<xs:complexType name="Milestones">
		<xs:sequence/>
		<xs:attribute name="Name" type="xs:string" use="required"/>
		<xs:attribute name="Status" type="xs:string" use="required"/>
		<xs:attribute name="DateOccurred" type="xs:dateTime" use="required"/>
		<xs:attribute name="StageID" type="xs:long" use="required"/>
</xs:complexType>
<xs:complexType name="MilestonesFilter">
		<xs:sequence/>
		<xs:attribute name="Name" type="xs:string" use="required"/>
		<xs:attribute name="Status" type="xs:string" use="required"/>
		<xs:attribute name="DateOccurred" type="xs:string" use="required"/>
		<xs:attribute name="StageID" type="xs:string" use="required"/>
</xs:complexType>
<xs:complexType name="ABOEvent">
		<xs:sequence/>
		<xs:attribute name="Name" type="xs:string" use="required"/>
		<xs:attribute name="Type" type="xs:string" use="required"/>
		<xs:attribute name="SubType" type="xs:string" use="required"/>
		<xs:attribute name="CreationTime" type="xs:dateTime" use="required"/>
		<xs:attribute name="CompletionTime" type="xs:dateTime" use="required"/>
		<xs:attribute name="Status" type="xs:string" use="required"/>
		<xs:attribute name="WorkEventCreationTime" type="xs:dateTime" use="required"/>
		<xs:attribute name="WorkEventID" type="xs:long" use="required"/>
</xs:complexType>
<xs:complexType name="ABOEventFilter">
		<xs:sequence/>
		<xs:attribute name="Name" type="xs:string" use="required"/>
		<xs:attribute name="Type" type="xs:string" use="required"/>
		<xs:attribute name="SubType" type="xs:string" use="required"/>
		<xs:attribute name="CreationTime" type="xs:string" use="required"/>
		<xs:attribute name="CompletionTime" type="xs:string" use="required"/>
		<xs:attribute name="Status" type="xs:string" use="required"/>
		<xs:attribute name="WorkEventCreationTime" type="xs:string" use="required"/>
		<xs:attribute name="WorkEventID" type="xs:string" use="required"/>
</xs:complexType>
<xs:complexType name="ABOSubscription">
		<xs:sequence/>
		<xs:attribute name="InterestedArtifactType" type="xs:string" use="required"/>
		<xs:attribute name="InterestedArtifactID" type="xs:long" use="required"/>
		<xs:attribute name="InterestedXpath" type="xs:string" use="required"/>
		<xs:attribute name="GuardOrMilestoneID" type="xs:string" use="required"/>
</xs:complexType>
<xs:complexType name="ABOSubscriptionFilter">
		<xs:sequence/>
		<xs:attribute name="InterestedArtifactType" type="xs:string" use="required"/>
		<xs:attribute name="InterestedArtifactID" type="xs:string" use="required"/>
		<xs:attribute name="InterestedXpath" type="xs:string" use="required"/>
		<xs:attribute name="GuardOrMilestoneID" type="xs:string" use="required"/>
</xs:complexType>
<xs:simpleType name="NewEntityRef" id="NewEntityRefID"><xs:annotation><xs:documentation>NewEntity</xs:documentation></xs:annotation><xs:restriction base="xs:long"><xs:minInclusive value="0"/></xs:restriction></xs:simpleType>
</xs:schema>

</xsl:template>
</xsl:stylesheet>