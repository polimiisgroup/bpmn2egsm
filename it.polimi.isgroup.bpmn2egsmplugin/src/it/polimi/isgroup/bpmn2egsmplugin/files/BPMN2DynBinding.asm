<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="BPMN2DynBinding"/>
		<constant value="links"/>
		<constant value="NTransientLinkSet;"/>
		<constant value="col"/>
		<constant value="J"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="OclParametrizedType"/>
		<constant value="#native"/>
		<constant value="Collection"/>
		<constant value="J.setName(S):V"/>
		<constant value="OclSimpleType"/>
		<constant value="OclAny"/>
		<constant value="J.setElementType(J):V"/>
		<constant value="TransientLinkSet"/>
		<constant value="A.__matcher__():V"/>
		<constant value="A.__exec__():V"/>
		<constant value="self"/>
		<constant value="__resolve__"/>
		<constant value="1"/>
		<constant value="J.oclIsKindOf(J):B"/>
		<constant value="18"/>
		<constant value="NTransientLinkSet;.getLinkBySourceElement(S):QNTransientLink;"/>
		<constant value="J.oclIsUndefined():B"/>
		<constant value="15"/>
		<constant value="NTransientLink;.getTargetFromSource(J):J"/>
		<constant value="17"/>
		<constant value="30"/>
		<constant value="Sequence"/>
		<constant value="2"/>
		<constant value="A.__resolve__(J):J"/>
		<constant value="QJ.including(J):QJ"/>
		<constant value="QJ.flatten():QJ"/>
		<constant value="e"/>
		<constant value="value"/>
		<constant value="resolveTemp"/>
		<constant value="S"/>
		<constant value="NTransientLink;.getNamedTargetFromSource(JS):J"/>
		<constant value="name"/>
		<constant value="__matcher__"/>
		<constant value="A.__matchprocess2mapping():V"/>
		<constant value="A.__matchdataobject2artifact():V"/>
		<constant value="__exec__"/>
		<constant value="process2mapping"/>
		<constant value="NTransientLinkSet;.getLinksByRule(S):QNTransientLink;"/>
		<constant value="A.__applyprocess2mapping(NTransientLink;):V"/>
		<constant value="dataobject2artifact"/>
		<constant value="A.__applydataobject2artifact(NTransientLink;):V"/>
		<constant value="getBindingEvents"/>
		<constant value="MBPMN!DataObject;"/>
		<constant value="OrderedSet"/>
		<constant value="DataOutputAssociation"/>
		<constant value="BPMN"/>
		<constant value="J.allInstances():J"/>
		<constant value="targetRef"/>
		<constant value="0"/>
		<constant value="J.=(J):J"/>
		<constant value="B.not():B"/>
		<constant value="24"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="sourceRef"/>
		<constant value="CatchEvent"/>
		<constant value="3"/>
		<constant value="dataOutputs"/>
		<constant value="J.includesAll(J):J"/>
		<constant value="51"/>
		<constant value="J.including(J):J"/>
		<constant value="J.flatten():J"/>
		<constant value="26:51-26:63"/>
		<constant value="25:2-25:28"/>
		<constant value="25:2-25:43"/>
		<constant value="25:59-25:60"/>
		<constant value="25:59-25:70"/>
		<constant value="25:73-25:77"/>
		<constant value="25:59-25:77"/>
		<constant value="25:2-25:78"/>
		<constant value="25:95-25:96"/>
		<constant value="25:95-25:106"/>
		<constant value="25:2-25:107"/>
		<constant value="27:3-27:6"/>
		<constant value="27:21-27:36"/>
		<constant value="27:21-27:51"/>
		<constant value="27:68-27:70"/>
		<constant value="27:68-27:82"/>
		<constant value="27:99-27:100"/>
		<constant value="27:68-27:101"/>
		<constant value="27:21-27:102"/>
		<constant value="27:3-27:103"/>
		<constant value="25:2-29:3"/>
		<constant value="25:2-30:14"/>
		<constant value="t"/>
		<constant value="s"/>
		<constant value="ce"/>
		<constant value="ret"/>
		<constant value="getUnbindingEvents"/>
		<constant value="DataInputAssociation"/>
		<constant value="J.includes(J):J"/>
		<constant value="ThrowEvent"/>
		<constant value="dataInputs"/>
		<constant value="34:51-34:63"/>
		<constant value="33:2-33:27"/>
		<constant value="33:2-33:42"/>
		<constant value="33:58-33:59"/>
		<constant value="33:58-33:69"/>
		<constant value="33:82-33:86"/>
		<constant value="33:58-33:87"/>
		<constant value="33:2-33:88"/>
		<constant value="33:105-33:106"/>
		<constant value="33:105-33:116"/>
		<constant value="33:2-33:117"/>
		<constant value="35:3-35:6"/>
		<constant value="35:21-35:36"/>
		<constant value="35:21-35:51"/>
		<constant value="35:68-35:70"/>
		<constant value="35:68-35:81"/>
		<constant value="35:95-35:96"/>
		<constant value="35:68-35:97"/>
		<constant value="35:21-35:98"/>
		<constant value="35:3-35:99"/>
		<constant value="33:2-37:3"/>
		<constant value="33:2-38:14"/>
		<constant value="produceBinding"/>
		<constant value="TransientLink"/>
		<constant value="NTransientLink;.setRule(MATL!Rule;):V"/>
		<constant value="id"/>
		<constant value="NTransientLink;.addSourceElement(SJ):V"/>
		<constant value="tb"/>
		<constant value="bindingEvent"/>
		<constant value="DB"/>
		<constant value="NTransientLink;.addTargetElement(SJ):V"/>
		<constant value="NTransientLinkSet;.addLink2(NTransientLink;B):V"/>
		<constant value="46:10-46:12"/>
		<constant value="46:4-46:12"/>
		<constant value="45:3-47:4"/>
		<constant value="produceUnbinding"/>
		<constant value="tub"/>
		<constant value="unbindingEvent"/>
		<constant value="55:10-55:12"/>
		<constant value="55:4-55:12"/>
		<constant value="54:3-56:4"/>
		<constant value="__matchprocess2mapping"/>
		<constant value="Process"/>
		<constant value="IN"/>
		<constant value="MMOF!Classifier;.allInstancesFrom(S):QJ"/>
		<constant value="sp"/>
		<constant value="tm"/>
		<constant value="mapping"/>
		<constant value="td"/>
		<constant value="definitions"/>
		<constant value="63:3-65:4"/>
		<constant value="67:3-69:4"/>
		<constant value="__applyprocess2mapping"/>
		<constant value="NTransientLink;"/>
		<constant value="NTransientLink;.getSourceElement(S):J"/>
		<constant value="NTransientLink;.getTargetElement(S):J"/>
		<constant value="4"/>
		<constant value="DataObject"/>
		<constant value="5"/>
		<constant value="ta"/>
		<constant value="J.resolveTemp(JJ):J"/>
		<constant value="artifact"/>
		<constant value="64:17-64:32"/>
		<constant value="64:17-64:47"/>
		<constant value="64:62-64:72"/>
		<constant value="64:85-64:86"/>
		<constant value="64:87-64:91"/>
		<constant value="64:62-64:92"/>
		<constant value="64:17-64:93"/>
		<constant value="64:4-64:94"/>
		<constant value="68:15-68:17"/>
		<constant value="68:4-68:17"/>
		<constant value="link"/>
		<constant value="__matchdataobject2artifact"/>
		<constant value="sdo"/>
		<constant value="77:3-81:4"/>
		<constant value="__applydataobject2artifact"/>
		<constant value="J.getBindingEvents():J"/>
		<constant value="J.produceBinding(J):J"/>
		<constant value="J.getUnbindingEvents():J"/>
		<constant value="J.produceUnbinding(J):J"/>
		<constant value="78:12-78:15"/>
		<constant value="78:12-78:20"/>
		<constant value="78:4-78:20"/>
		<constant value="79:20-79:23"/>
		<constant value="79:20-79:42"/>
		<constant value="79:59-79:69"/>
		<constant value="79:85-79:87"/>
		<constant value="79:85-79:90"/>
		<constant value="79:59-79:91"/>
		<constant value="79:20-79:92"/>
		<constant value="79:4-79:92"/>
		<constant value="80:22-80:25"/>
		<constant value="80:22-80:46"/>
		<constant value="80:64-80:74"/>
		<constant value="80:92-80:95"/>
		<constant value="80:92-80:98"/>
		<constant value="80:64-80:99"/>
		<constant value="80:22-80:100"/>
		<constant value="80:4-80:100"/>
		<constant value="be"/>
		<constant value="ube"/>
	</cp>
	<field name="1" type="2"/>
	<field name="3" type="4"/>
	<operation name="5">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<push arg="7"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="9"/>
			<pcall arg="10"/>
			<dup/>
			<push arg="11"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="12"/>
			<pcall arg="10"/>
			<pcall arg="13"/>
			<set arg="3"/>
			<getasm/>
			<push arg="14"/>
			<push arg="8"/>
			<new/>
			<set arg="1"/>
			<getasm/>
			<pcall arg="15"/>
			<getasm/>
			<pcall arg="16"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="24"/>
		</localvariabletable>
	</operation>
	<operation name="18">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
		</parameters>
		<code>
			<load arg="19"/>
			<getasm/>
			<get arg="3"/>
			<call arg="20"/>
			<if arg="21"/>
			<getasm/>
			<get arg="1"/>
			<load arg="19"/>
			<call arg="22"/>
			<dup/>
			<call arg="23"/>
			<if arg="24"/>
			<load arg="19"/>
			<call arg="25"/>
			<goto arg="26"/>
			<pop/>
			<load arg="19"/>
			<goto arg="27"/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<load arg="19"/>
			<iterate/>
			<store arg="29"/>
			<getasm/>
			<load arg="29"/>
			<call arg="30"/>
			<call arg="31"/>
			<enditerate/>
			<call arg="32"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="33" begin="23" end="27"/>
			<lve slot="0" name="17" begin="0" end="29"/>
			<lve slot="1" name="34" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="35">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
			<parameter name="29" type="36"/>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<load arg="19"/>
			<call arg="22"/>
			<load arg="19"/>
			<load arg="29"/>
			<call arg="37"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="6"/>
			<lve slot="1" name="34" begin="0" end="6"/>
			<lve slot="2" name="38" begin="0" end="6"/>
		</localvariabletable>
	</operation>
	<operation name="39">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<pcall arg="40"/>
			<getasm/>
			<pcall arg="41"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="3"/>
		</localvariabletable>
	</operation>
	<operation name="42">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<push arg="43"/>
			<call arg="44"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="45"/>
			<enditerate/>
			<getasm/>
			<get arg="1"/>
			<push arg="46"/>
			<call arg="44"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="47"/>
			<enditerate/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="33" begin="5" end="8"/>
			<lve slot="1" name="33" begin="15" end="18"/>
			<lve slot="0" name="17" begin="0" end="19"/>
		</localvariabletable>
	</operation>
	<operation name="48">
		<context type="49"/>
		<parameters>
		</parameters>
		<code>
			<push arg="50"/>
			<push arg="8"/>
			<new/>
			<store arg="19"/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="51"/>
			<push arg="52"/>
			<findme/>
			<call arg="53"/>
			<iterate/>
			<store arg="29"/>
			<load arg="29"/>
			<get arg="54"/>
			<load arg="55"/>
			<call arg="56"/>
			<call arg="57"/>
			<if arg="58"/>
			<load arg="29"/>
			<call arg="59"/>
			<enditerate/>
			<iterate/>
			<store arg="29"/>
			<load arg="29"/>
			<get arg="60"/>
			<call arg="59"/>
			<enditerate/>
			<iterate/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="61"/>
			<push arg="52"/>
			<findme/>
			<call arg="53"/>
			<iterate/>
			<store arg="62"/>
			<load arg="62"/>
			<get arg="63"/>
			<load arg="29"/>
			<call arg="64"/>
			<call arg="57"/>
			<if arg="65"/>
			<load arg="62"/>
			<call arg="59"/>
			<enditerate/>
			<call arg="66"/>
			<store arg="19"/>
			<enditerate/>
			<load arg="19"/>
			<call arg="67"/>
		</code>
		<linenumbertable>
			<lne id="68" begin="0" end="2"/>
			<lne id="69" begin="10" end="12"/>
			<lne id="70" begin="10" end="13"/>
			<lne id="71" begin="16" end="16"/>
			<lne id="72" begin="16" end="17"/>
			<lne id="73" begin="18" end="18"/>
			<lne id="74" begin="16" end="19"/>
			<lne id="75" begin="7" end="24"/>
			<lne id="76" begin="27" end="27"/>
			<lne id="77" begin="27" end="28"/>
			<lne id="78" begin="4" end="30"/>
			<lne id="79" begin="33" end="33"/>
			<lne id="80" begin="37" end="39"/>
			<lne id="81" begin="37" end="40"/>
			<lne id="82" begin="43" end="43"/>
			<lne id="83" begin="43" end="44"/>
			<lne id="84" begin="45" end="45"/>
			<lne id="85" begin="43" end="46"/>
			<lne id="86" begin="34" end="51"/>
			<lne id="87" begin="33" end="52"/>
			<lne id="88" begin="0" end="55"/>
			<lne id="89" begin="0" end="56"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="90" begin="15" end="23"/>
			<lve slot="2" name="91" begin="26" end="29"/>
			<lve slot="3" name="92" begin="42" end="50"/>
			<lve slot="2" name="33" begin="32" end="53"/>
			<lve slot="1" name="93" begin="3" end="55"/>
			<lve slot="0" name="17" begin="0" end="56"/>
		</localvariabletable>
	</operation>
	<operation name="94">
		<context type="49"/>
		<parameters>
		</parameters>
		<code>
			<push arg="50"/>
			<push arg="8"/>
			<new/>
			<store arg="19"/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="95"/>
			<push arg="52"/>
			<findme/>
			<call arg="53"/>
			<iterate/>
			<store arg="29"/>
			<load arg="29"/>
			<get arg="60"/>
			<load arg="55"/>
			<call arg="96"/>
			<call arg="57"/>
			<if arg="58"/>
			<load arg="29"/>
			<call arg="59"/>
			<enditerate/>
			<iterate/>
			<store arg="29"/>
			<load arg="29"/>
			<get arg="54"/>
			<call arg="59"/>
			<enditerate/>
			<iterate/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="97"/>
			<push arg="52"/>
			<findme/>
			<call arg="53"/>
			<iterate/>
			<store arg="62"/>
			<load arg="62"/>
			<get arg="98"/>
			<load arg="29"/>
			<call arg="96"/>
			<call arg="57"/>
			<if arg="65"/>
			<load arg="62"/>
			<call arg="59"/>
			<enditerate/>
			<call arg="66"/>
			<store arg="19"/>
			<enditerate/>
			<load arg="19"/>
			<call arg="67"/>
		</code>
		<linenumbertable>
			<lne id="99" begin="0" end="2"/>
			<lne id="100" begin="10" end="12"/>
			<lne id="101" begin="10" end="13"/>
			<lne id="102" begin="16" end="16"/>
			<lne id="103" begin="16" end="17"/>
			<lne id="104" begin="18" end="18"/>
			<lne id="105" begin="16" end="19"/>
			<lne id="106" begin="7" end="24"/>
			<lne id="107" begin="27" end="27"/>
			<lne id="108" begin="27" end="28"/>
			<lne id="109" begin="4" end="30"/>
			<lne id="110" begin="33" end="33"/>
			<lne id="111" begin="37" end="39"/>
			<lne id="112" begin="37" end="40"/>
			<lne id="113" begin="43" end="43"/>
			<lne id="114" begin="43" end="44"/>
			<lne id="115" begin="45" end="45"/>
			<lne id="116" begin="43" end="46"/>
			<lne id="117" begin="34" end="51"/>
			<lne id="118" begin="33" end="52"/>
			<lne id="119" begin="0" end="55"/>
			<lne id="120" begin="0" end="56"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="90" begin="15" end="23"/>
			<lve slot="2" name="91" begin="26" end="29"/>
			<lve slot="3" name="92" begin="42" end="50"/>
			<lve slot="2" name="33" begin="32" end="53"/>
			<lve slot="1" name="93" begin="3" end="55"/>
			<lve slot="0" name="17" begin="0" end="56"/>
		</localvariabletable>
	</operation>
	<operation name="121">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="36"/>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<push arg="122"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="121"/>
			<pcall arg="123"/>
			<dup/>
			<push arg="124"/>
			<load arg="19"/>
			<pcall arg="125"/>
			<dup/>
			<push arg="126"/>
			<push arg="127"/>
			<push arg="128"/>
			<new/>
			<dup/>
			<store arg="29"/>
			<pcall arg="129"/>
			<pushf/>
			<pcall arg="130"/>
			<load arg="29"/>
			<dup/>
			<getasm/>
			<load arg="19"/>
			<call arg="30"/>
			<set arg="124"/>
			<pop/>
			<load arg="29"/>
		</code>
		<linenumbertable>
			<lne id="131" begin="25" end="25"/>
			<lne id="132" begin="23" end="27"/>
			<lne id="133" begin="22" end="28"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="126" begin="18" end="29"/>
			<lve slot="0" name="17" begin="0" end="29"/>
			<lve slot="1" name="124" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="134">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="36"/>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<push arg="122"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="134"/>
			<pcall arg="123"/>
			<dup/>
			<push arg="124"/>
			<load arg="19"/>
			<pcall arg="125"/>
			<dup/>
			<push arg="135"/>
			<push arg="136"/>
			<push arg="128"/>
			<new/>
			<dup/>
			<store arg="29"/>
			<pcall arg="129"/>
			<pushf/>
			<pcall arg="130"/>
			<load arg="29"/>
			<dup/>
			<getasm/>
			<load arg="19"/>
			<call arg="30"/>
			<set arg="124"/>
			<pop/>
			<load arg="29"/>
		</code>
		<linenumbertable>
			<lne id="137" begin="25" end="25"/>
			<lne id="138" begin="23" end="27"/>
			<lne id="139" begin="22" end="28"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="135" begin="18" end="29"/>
			<lve slot="0" name="17" begin="0" end="29"/>
			<lve slot="1" name="124" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="140">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="141"/>
			<push arg="52"/>
			<findme/>
			<push arg="142"/>
			<call arg="143"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="122"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="43"/>
			<pcall arg="123"/>
			<dup/>
			<push arg="144"/>
			<load arg="19"/>
			<pcall arg="125"/>
			<dup/>
			<push arg="145"/>
			<push arg="146"/>
			<push arg="128"/>
			<new/>
			<pcall arg="129"/>
			<dup/>
			<push arg="147"/>
			<push arg="148"/>
			<push arg="128"/>
			<new/>
			<pcall arg="129"/>
			<pusht/>
			<pcall arg="130"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="149" begin="19" end="24"/>
			<lne id="150" begin="25" end="30"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="144" begin="6" end="32"/>
			<lve slot="0" name="17" begin="0" end="33"/>
		</localvariabletable>
	</operation>
	<operation name="151">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="152"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="144"/>
			<call arg="153"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="145"/>
			<call arg="154"/>
			<store arg="62"/>
			<load arg="19"/>
			<push arg="147"/>
			<call arg="154"/>
			<store arg="155"/>
			<load arg="62"/>
			<dup/>
			<getasm/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<push arg="156"/>
			<push arg="52"/>
			<findme/>
			<call arg="53"/>
			<iterate/>
			<store arg="157"/>
			<getasm/>
			<load arg="157"/>
			<push arg="158"/>
			<call arg="159"/>
			<call arg="59"/>
			<enditerate/>
			<call arg="30"/>
			<set arg="160"/>
			<pop/>
			<load arg="155"/>
			<dup/>
			<getasm/>
			<load arg="62"/>
			<call arg="30"/>
			<set arg="146"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="161" begin="18" end="20"/>
			<lne id="162" begin="18" end="21"/>
			<lne id="163" begin="24" end="24"/>
			<lne id="164" begin="25" end="25"/>
			<lne id="165" begin="26" end="26"/>
			<lne id="166" begin="24" end="27"/>
			<lne id="167" begin="15" end="29"/>
			<lne id="168" begin="13" end="31"/>
			<lne id="149" begin="12" end="32"/>
			<lne id="169" begin="36" end="36"/>
			<lne id="170" begin="34" end="38"/>
			<lne id="150" begin="33" end="39"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="5" name="33" begin="23" end="28"/>
			<lve slot="3" name="145" begin="7" end="39"/>
			<lve slot="4" name="147" begin="11" end="39"/>
			<lve slot="2" name="144" begin="3" end="39"/>
			<lve slot="0" name="17" begin="0" end="39"/>
			<lve slot="1" name="171" begin="0" end="39"/>
		</localvariabletable>
	</operation>
	<operation name="172">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="156"/>
			<push arg="52"/>
			<findme/>
			<push arg="142"/>
			<call arg="143"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="122"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="46"/>
			<pcall arg="123"/>
			<dup/>
			<push arg="173"/>
			<load arg="19"/>
			<pcall arg="125"/>
			<dup/>
			<push arg="158"/>
			<push arg="160"/>
			<push arg="128"/>
			<new/>
			<pcall arg="129"/>
			<pusht/>
			<pcall arg="130"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="174" begin="19" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="173" begin="6" end="26"/>
			<lve slot="0" name="17" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="175">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="152"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="173"/>
			<call arg="153"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="158"/>
			<call arg="154"/>
			<store arg="62"/>
			<load arg="62"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<call arg="30"/>
			<set arg="38"/>
			<dup/>
			<getasm/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<load arg="29"/>
			<call arg="176"/>
			<iterate/>
			<store arg="155"/>
			<getasm/>
			<load arg="155"/>
			<get arg="124"/>
			<call arg="177"/>
			<call arg="59"/>
			<enditerate/>
			<call arg="30"/>
			<set arg="127"/>
			<dup/>
			<getasm/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<load arg="29"/>
			<call arg="178"/>
			<iterate/>
			<store arg="155"/>
			<getasm/>
			<load arg="155"/>
			<get arg="124"/>
			<call arg="179"/>
			<call arg="59"/>
			<enditerate/>
			<call arg="30"/>
			<set arg="136"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="180" begin="11" end="11"/>
			<lne id="181" begin="11" end="12"/>
			<lne id="182" begin="9" end="14"/>
			<lne id="183" begin="20" end="20"/>
			<lne id="184" begin="20" end="21"/>
			<lne id="185" begin="24" end="24"/>
			<lne id="186" begin="25" end="25"/>
			<lne id="187" begin="25" end="26"/>
			<lne id="188" begin="24" end="27"/>
			<lne id="189" begin="17" end="29"/>
			<lne id="190" begin="15" end="31"/>
			<lne id="191" begin="37" end="37"/>
			<lne id="192" begin="37" end="38"/>
			<lne id="193" begin="41" end="41"/>
			<lne id="194" begin="42" end="42"/>
			<lne id="195" begin="42" end="43"/>
			<lne id="196" begin="41" end="44"/>
			<lne id="197" begin="34" end="46"/>
			<lne id="198" begin="32" end="48"/>
			<lne id="174" begin="8" end="49"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="4" name="199" begin="23" end="28"/>
			<lve slot="4" name="200" begin="40" end="45"/>
			<lve slot="3" name="158" begin="7" end="49"/>
			<lve slot="2" name="173" begin="3" end="49"/>
			<lve slot="0" name="17" begin="0" end="49"/>
			<lve slot="1" name="171" begin="0" end="49"/>
		</localvariabletable>
	</operation>
</asm>
