package it.polimi.isgroup.bpmn2egsmplugin.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.m2m.atl.common.ATLExecutionException;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class Translator {

	/**
	 * The main method.
	 * 
	 * @param args
	 *            are the arguments
	 * @generated
	 */
//	public static void main(String[] args) {
//		try {
//			if (args.length < 2) {
//				System.out.println("Arguments not valid : {IN_model_path, OUT_model_path}.");
//			} else {
//				Translator t = new Translator();
//				BPMN2XGSM bpmn2egsm = new BPMN2XGSM();
//				String processName = t.getProcessName(args[0]);
//				File procFile = new File(".temp/process.bpmn.xmi");
//				//delete previous temp folder
//				//new File(args[0]+".temp/"+processName+"/data/"+processName+"_DataTypes.xsd").deleteOnExit();
//				Files.deleteIfExists(procFile.toPath());
//				Files.deleteIfExists(new File(".temp/"+processName+"/siena.xml").toPath());
//				Files.deleteIfExists(new File(".temp/process.egsm.xmi").toPath());
//				Files.deleteIfExists(new File(".temp/"+processName+"/data/infoModel.xsd").toPath());
////				new File(".temp/"+processName+"/data").deleteOnExit();
////				new File(".temp/"+processName).deleteOnExit();
////				new File(".temp").deleteOnExit();
//				Files.deleteIfExists(new File(".temp/"+processName+"/data").toPath());
//				Files.deleteIfExists(new File(".temp/"+processName).toPath());
//				Files.deleteIfExists(new File(".temp").toPath());
//				//create temporary folders
//				boolean success = (new File(".temp")).mkdirs();
//				if (!success) {
//				    System.out.println("Cannot write to destination folder");
//				    return;
//				}
//				System.out.println("Temp folder created");
//			    success = (new File(".temp/"+processName)).mkdirs();
//				if (!success) {
//				    return;
//				}
//				success = (new File(".temp/"+processName+"/data")).mkdirs();
//				if (!success) {
//				    return;
//				}
//				//BPMN to XMI translation
//				t.transformXML(args[0], ".temp/process.bpmn.xmi", "BPMN20-ToXMI.xsl");
//				//ATL translation
//				bpmn2egsm.loadModels(".temp/process.bpmn.xmi");
//				bpmn2egsm.doBPMN2XGSM(new NullProgressMonitor());
//				bpmn2egsm.saveModels(".temp/process.egsm.xmi");
//				bpmn2egsm = null;
//				//XMI to Siena translation
//				t.transformXML(".temp/process.egsm.xmi", ".temp/"+processName+"/siena.xml", "xmi2siena.xsl");
//				t.transformXML(".temp/process.egsm.xmi", ".temp/"+processName+"/data/infoModel.xsd", "xmi2xsd.xsl");
//				//runner.transformXML(args[0]+".temp/"+args[0]+".egsm.xmi", args[0]+".temp/"+processName+"/data/"+processName+"_DataTypes.xsd", "xmi2dtxsd.xsl");
//				//Siena project produced
//				if(args.length>2) {
//					for(int i=0;i<args.length-2;i++){
//						Files.copy(new File(".temp/"+processName+"/siena.xml").toPath(), new File(".temp/siena.xml").toPath(), StandardCopyOption.REPLACE_EXISTING);
//						FSM2EGSM fsm2egsm = new FSM2EGSM();
//						//ATL translation
//						fsm2egsm.loadModels(args[1+i]);
//						fsm2egsm.doFSM2EGSM(new NullProgressMonitor());
//						fsm2egsm.saveModels(".temp/lifecycle.egsm.xmi");
//						t.transformXML(".temp/lifecycle.egsm.xmi", ".temp/"+processName+"/siena.xml", "xmi2siena_merge.xsl");
//						Files.delete(new File(".temp/siena.xml").toPath());
//						Files.delete(new File(".temp/lifecycle.egsm.xmi").toPath());
//					}
//				}
//				ArrayList<String> files = new ArrayList<String>();
//				files.add(processName+"/siena.xml");
//				files.add(processName+"/data/infoModel.xsd");
//				//files.add(processName+"/data/"+processName+"_DataTypes.xsd");
//				t.zipFolder(".temp", files, args[args.length-1]+".zip");
//				t = null;
//				files = null;
//				//cleanup
//				new File(".temp/process.bpmn.xmi").deleteOnExit();
//				new File(".temp/"+processName+"/siena.xml").deleteOnExit();
//				new File(".temp/process.egsm.xmi").deleteOnExit();
//				//new File(args[0]+".temp/"+processName+"/data/"+processName+"_DataTypes.xsd").deleteOnExit();
//				new File(".temp/"+processName+"/data/infoModel.xsd").deleteOnExit();
//				new File(".temp/"+processName+"/data").deleteOnExit();
//				new File(".temp/"+processName).deleteOnExit();
//				new File(".temp").deleteOnExit();
//			}
//		} catch (ATLCoreException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (ATLExecutionException e) {
//			e.printStackTrace();
//		}
//	}
//
	public static void main(String[] args) {
		try {
			if (args.length < 1) {
				System.out.println("Usage: java -jar bpmn2egsm.jar <BPMN_model> [<FSM_model>] [-lb]");
				System.out.println("<BPMN_model> : path to source BPMN model");
				System.out.println("<FSM_model> : path to source artifact lifecycle UML State Chart diagram");
				System.out.println("-lb : generates artifact-to-object mapping criteria");
				
			} else {
				Translator t = new Translator();
				BPMN2XGSM bpmn2egsm = new BPMN2XGSM();
				String processName = t.getProcessName(args[0]);
				//File procFile = new File(processName+"/process.bpmn.xmi");
				//create temporary folders
				boolean success = (new File(processName)).mkdirs();
				if (!success) {
				    System.out.println("Cannot write to destination folder");
				    return;
				}
				System.out.println("Temp folder created");
			    //BPMN to XMI translation
				t.transformXML(args[0], processName+"/process.bpmn.xmi", "BPMN20-ToXMI.xsl");
				//ATL translation
				bpmn2egsm.loadModels(processName+"/process.bpmn.xmi");
				System.out.println("Transforming process model");
				bpmn2egsm.doBPMN2XGSM(new NullProgressMonitor());
				bpmn2egsm.saveModels(processName+"/process.egsm.xmi");
				bpmn2egsm = null;
				//XMI to Siena translation
				t.transformXML(processName+"/process.egsm.xmi", processName+"/egsm.xml", "xmi2siena.xsl");
				t.transformXML(processName+"/process.egsm.xmi", processName+"/infoModel.xsd", "xmi2xsd.xsl");
				//Siena project produced
				int lastfsm;
				if(args.length>1) {
					if (args[args.length-1].equals("-lb")){
						BPMN2DynBinding bpmn2db = new BPMN2DynBinding();
						bpmn2db.loadModels(processName+"/process.bpmn.xmi");
						System.out.println("Generating artifact-to-object mapping criteria");
						bpmn2db.doBPMN2DynBinding(new NullProgressMonitor());
						bpmn2db.saveModels(processName+"/binding.xmi");
						t.transformXML(processName+"/binding.xmi", processName+"/binding.xml", "xmi2dynBinding.xsl");
						Files.delete(new File(processName+"/binding.xmi").toPath());
						lastfsm = args.length-1;
					} else {
						lastfsm = args.length;
					}
					for(int i=1;i<lastfsm;i++){
						Files.copy(new File(processName+"/egsm.xml").toPath(), new File("egsm-temp.xml").toPath(), StandardCopyOption.REPLACE_EXISTING);
						FSM2EGSM fsm2egsm = new FSM2EGSM();
						//ATL translation
						fsm2egsm.loadModels(args[i]);
						System.out.println("Transforming artifact lifecycle model");
						fsm2egsm.doFSM2EGSM(new NullProgressMonitor());
						fsm2egsm.saveModels(processName+"/lifecycle.egsm.xmi");
						t.transformXML(processName+"/lifecycle.egsm.xmi", processName+"/egsm.xml", "xmi2siena_merge.xsl");
						Files.delete(new File("egsm-temp.xml").toPath());
						Files.delete(new File(processName+"/lifecycle.egsm.xmi").toPath());
					}
				}
				Files.delete(new File(processName+"/process.egsm.xmi").toPath());
				Files.delete(new File(processName+"/process.bpmn.xmi").toPath());
				System.out.println("All transformations completed successfully");
			}
		} catch (ATLCoreException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ATLExecutionException e) {
			e.printStackTrace();
		}
	}


	public String getProcessName(String filename){
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new File(filename));
        doc.getDocumentElement().normalize();
        return doc.getDocumentElement().getElementsByTagName("bpmn2:process").item(0).getAttributes().getNamedItem("id").getNodeValue();

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filename;
	}
	
	public void transformXML(String inFilename, String outFilename, String xslFilename){
		try {
            // Create transformer factory
            TransformerFactory factory = TransformerFactory.newInstance();

            // Use the factory to create a template containing the xsl file
            Templates template = factory.newTemplates(new StreamSource(
                new FileInputStream(xslFilename)));

            // Use the template to create a transformer
            Transformer xformer = template.newTransformer();

            // Prepare the input and output files
            Source source = new StreamSource(new FileInputStream(inFilename));
            Result result = new StreamResult(new FileOutputStream(outFilename));

            // Apply the xsl file to the source file and write the result to the output file
            xformer.transform(source, result);
        } catch (FileNotFoundException e) {
        	
        } catch (TransformerConfigurationException e) {
            // An error occurred in the XSL file
        } catch (TransformerException e) {
            // An error occurred while applying the XSL file
            // Get location of error in input file
            /*
        	SourceLocator locator = e.getLocator();
            int col = locator.getColumnNumber();
            int line = locator.getLineNumber();
            String publicId = locator.getPublicId();
            String systemId = locator.getSystemId();
            */
        }
	}

	public void zipFolder(String folder, List<String> files, String zipFile)
	{
	   byte[] buffer = new byte[1024];
	   FileOutputStream fos = null;
	   ZipOutputStream zos = null;
	   try
	   {
	     fos = new FileOutputStream(zipFile);
	     zos = new ZipOutputStream(fos);

	     System.out.println("Output to Zip : " + zipFile);
	     FileInputStream in = null;

	     for (String file : files)
	     {
	        System.out.println("File Added : " + file);
	        ZipEntry ze = new ZipEntry(file);
	        zos.putNextEntry(ze);
	        try
	        {
	           in = new FileInputStream(folder + File.separator + file);
	           int len;
	           while ((len = in.read(buffer)) > 0)
	           {
	              zos.write(buffer, 0, len);
	           }
	        }
	        finally
	        {
	           in.close();
	        }
	     }

	     zos.closeEntry();
	     System.out.println("Folder successfully compressed");

	  }
	  catch (IOException ex)
	  {
	     ex.printStackTrace();
	  }
	  finally
	  {
	     try
	     {
	        zos.close();
	     }
	     catch (IOException e)
	     {
	        e.printStackTrace();
	     }
	  }
	}
	
	
}
