<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:martifact="http://foo.bar.it/martifact">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
  
 <!-- keep comments -->
<xsl:template match="comment()">
    <xsl:copy>
      <xsl:apply-templates/>
    </xsl:copy>
</xsl:template>

<!-- fix namespaces -->  
<xsl:template match="*">
    <!-- remove element prefix -->
    <xsl:element name="martifact:{local-name()}" namespace="http://foo.bar.it/martifact">
      <!-- process attributes -->
      <xsl:for-each select="@*">
        <!-- remove attribute prefix -->
        <xsl:attribute name="{local-name()}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>
      <xsl:apply-templates/>
    </xsl:element>
</xsl:template>

<!-- fix tag -->
<xsl:template match="localArtifact">
  <martifact:localArtifact>
    <xsl:for-each select="@*">
        <xsl:attribute name="{local-name()}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>
    <xsl:apply-templates select="node()"/>
  </martifact:localArtifact>
</xsl:template>

<!-- fix tag -->
<xsl:template match="mapping">
  <martifact:mapping>
    <xsl:for-each select="@*">
        <xsl:attribute name="{local-name()}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>
    <xsl:apply-templates select="node()"/>
  </martifact:mapping>
</xsl:template>

<!-- fix tag -->
<xsl:template match="artifact">
  <martifact:artifact>
    <xsl:for-each select="@*">
        <xsl:attribute name="{local-name()}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>
    <xsl:apply-templates select="node()"/>
  </martifact:artifact>
</xsl:template>

<!-- fix tag -->
<xsl:template match="bindingEvent">
  <martifact:bindingEvent>
    <xsl:for-each select="@*">
        <xsl:attribute name="{local-name()}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>
    <xsl:apply-templates select="node()"/>
  </martifact:bindingEvent>
</xsl:template>

<!-- fix tag -->
<xsl:template match="unbindingEvent">
  <martifact:unbindingEvent>
    <xsl:for-each select="@*">
        <xsl:attribute name="{local-name()}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>
    <xsl:apply-templates select="node()"/>
  </martifact:unbindingEvent>
</xsl:template>

<!-- fix tag -->
<xsl:template match="stakeholder">
  <martifact:stakeholder>
    <xsl:for-each select="@*">
        <xsl:attribute name="{local-name()}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>
    <xsl:apply-templates select="node()"/>
  </martifact:stakeholder>
</xsl:template>


</xsl:stylesheet>